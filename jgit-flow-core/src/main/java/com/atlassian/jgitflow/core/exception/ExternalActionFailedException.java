package com.atlassian.jgitflow.core.exception;

/**
 * Exception can be thrown when ExternalAction call fails
 */
public class ExternalActionFailedException extends JGitFlowException
{
    public ExternalActionFailedException()
    {
    }

    public ExternalActionFailedException(Throwable cause)
    {
        super(cause);
    }

    public ExternalActionFailedException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ExternalActionFailedException(String message)
    {
        super(message);
    }
}
